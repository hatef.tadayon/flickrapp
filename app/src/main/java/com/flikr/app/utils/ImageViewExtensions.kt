package com.flikr.app.utils

import android.view.View
import android.widget.ImageView

fun ImageView.setImageFromUrl(imageUrl: String) {
    GlideApp.with(context).load(imageUrl).thumbnail(0.1f).into(this)
}

fun ImageView.setImageFromUrlWithProgressBar(url: String, progress: View) {
    GlideApp.with(this.context).load(url).thumbnail(0.1f).listener(PhotosRequestListener(progress))
        .into(this)
}