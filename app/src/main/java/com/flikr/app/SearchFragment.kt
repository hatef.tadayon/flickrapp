package com.flikr.app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.flikr.app.data.Result
import com.flikr.app.data.api.ApiService
import com.flikr.app.data.api.RetrofitService
import com.flikr.app.data.model.Photo
import com.flikr.app.databinding.FragmentSearchBinding
import com.flikr.app.ui.InfiniteScrollListener
import com.flikr.app.ui.PhotosAdapter
import com.flikr.app.ui.PhotosViewModel
import com.flikr.app.ui.ViewModelFactory
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class SearchFragment : Fragment() {

    private var photosViewModel: PhotosViewModel? = null
    private lateinit var binding: FragmentSearchBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater)
        binding.lifecycleOwner = this@SearchFragment

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBindings()
        initViewModels()
        initObservers()
        initListeners()
    }

    private fun initListeners() {
        search.isSubmitButtonEnabled = true
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    photosViewModel?.setSearchQuery(it)
                }
                search.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        }
        )
    }

    private fun initBindings() {
        val linearLayoutManager = LinearLayoutManager(activity)
        binding.homePhotosList.run {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
        }
    }

    private fun initViewModels() {
        if (null == photosViewModel) {
            photosViewModel = ViewModelProvider(
                this@SearchFragment,
                ViewModelFactory(RetrofitService.createService(ApiService::class.java))
            ).get(
                PhotosViewModel::class.java
            )

        }
    }

    private fun initObservers() {

        photosViewModel?.getPhotos()?.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Result.Success -> {
                    renderList(result.data)
                    binding.homePhotosProgressContainer.visibility = View.GONE
                    binding.homePhotosList.visibility = View.VISIBLE
                    binding.image.visibility = View.GONE
                    binding.imageText.visibility = View.GONE
                }
                is Result.InProgress -> {
                    binding.homePhotosProgressContainer.visibility = View.VISIBLE
                    binding.homePhotosList.visibility = View.GONE
                    binding.image.visibility = View.GONE
                    binding.imageText.visibility = View.GONE
                }
                is Result.NewSearch -> {
                    binding.homePhotosProgressContainer.visibility = View.VISIBLE
                    binding.image.visibility = View.VISIBLE
                    binding.imageText.visibility = View.VISIBLE
                    binding.homePhotosList.visibility = View.GONE
                    setRecyclerData(arrayListOf())
                    binding.homePhotosList.adapter?.notifyDataSetChanged()
                }
                is Result.Error -> {
                    binding.homePhotosProgressContainer.visibility = View.GONE
                    Toast.makeText(activity, result.exception.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun fetchMoreData() {
        photosViewModel?.loadDataNextPage()
    }

    private fun renderList(photos: ArrayList<Photo>) {
        if (photos.isNotEmpty()) {
            //when screen starts
            if (photosViewModel?.getCurrentPage() == 1 || binding.homePhotosList.adapter?.itemCount == 0) {
                setRecyclerData(photos)
            } else { //when load more
                if (binding.homePhotosList.adapter == null) { //after load more
                    setRecyclerData(photos)
                }
                binding.homePhotosList.adapter?.notifyDataSetChanged()
            }
            //load state of rv
            if (photosViewModel?.listState != null) {
                binding.homePhotosList.layoutManager?.onRestoreInstanceState(photosViewModel?.listState)
                photosViewModel?.listState = null
            }
        } else {
            showSnackBarMessage()
        }
    }

    private fun setRecyclerData(photos: ArrayList<Photo>) {
        with(binding.homePhotosList) {
            adapter = PhotosAdapter(photos)
            addOnScrollListener(
                InfiniteScrollListener(
                    { fetchMoreData() },
                    layoutManager as LinearLayoutManager
                )
            )
        }
    }

    private fun showSnackBarMessage() {
        Snackbar.make(view!!, R.string.no_data_msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        photosViewModel?.listState = binding.homePhotosList.layoutManager?.onSaveInstanceState()
        super.onDestroyView()
    }
}