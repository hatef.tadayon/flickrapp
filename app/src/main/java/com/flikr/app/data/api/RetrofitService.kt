package com.flikr.app.data.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitService {

    private const val BASE_URL = "https://api.flickr.com/services/rest/"
    var client: OkHttpClient
    private var retrofit: Retrofit
    var gson: Gson = GsonBuilder()
        .setLenient()
        .create()

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson)).client(client).build()
    }

    fun <T> createService(serviceClass: Class<T>): T = retrofit.create(serviceClass)
}