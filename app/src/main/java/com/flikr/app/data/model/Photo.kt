package com.flikr.app.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Photo(
    @SerializedName("id") val id: String,
    @SerializedName("secret") val secret: String,
    @SerializedName("title") val title: String,
    @SerializedName("server") val server: String,
    @SerializedName("farm") val farm: Int
) : Parcelable {

    fun getImageUrl(): String {
        return java.lang.String.format(
            IMAGE_URL,
            farm,
            server,
            id,
            secret
        )
    }

    companion object {
        private const val IMAGE_URL = "https://farm%s.staticflickr.com/%s/%s_%s_z.jpg"
    }
}

@Parcelize
data class PhotoDetail(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: Content,
    @SerializedName("description") val description: Content,
    @SerializedName("owner") val owner: Owner
) : Parcelable

@Parcelize
data class Owner(
    @SerializedName("username") val username: String,
    @SerializedName("realname") val realname: String
) : Parcelable

@Parcelize
data class Content(@SerializedName("_content") val content: String) : Parcelable