package com.flikr.app.data.model

import com.google.gson.annotations.SerializedName

data class PhotosResponse(
    @SerializedName("photos") val photos: PhotosListResponse
)

data class PhotosListResponse(@SerializedName("photo") val photo: ArrayList<Photo>,
                              @SerializedName("page") val page: Int,
                              @SerializedName("pages") val pages: Int,
                              @SerializedName("perpage") val perPage: Int,
                              @SerializedName("total") val total: Int)

data class PhotoDetailsResponse(
    @SerializedName("photo") val photo: PhotoDetail
)