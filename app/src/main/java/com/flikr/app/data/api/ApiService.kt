package com.flikr.app.data.api

import com.flikr.app.data.model.PhotoDetail
import com.flikr.app.data.model.PhotoDetailsResponse
import com.flikr.app.data.model.PhotosResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("?method=flickr.photos.search&format=json&nojsoncallback=1")
    suspend fun getPhotos(
        @Query("text") text: String,
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Response<PhotosResponse>?

    @GET("?method=flickr.photos.getInfo&format=json&nojsoncallback=1")
    suspend fun getPhotoDetail(
        @Query("photo_id") text: String,
        @Query("api_key") apiKey: String
    ): Response<PhotoDetailsResponse>?
}