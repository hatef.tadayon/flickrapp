package com.flikr.app.data.repository

import com.flikr.app.BuildConfig
import com.flikr.app.data.api.ApiService
import com.flikr.app.data.model.Photo
import retrofit2.HttpException
import com.flikr.app.data.Result
import com.flikr.app.data.model.PhotoDetail
import com.google.gson.JsonSyntaxException

class FlickrRepository(private val apiService: ApiService) : BaseRepository() {

    companion object {
        private val TAG = FlickrRepository::class.java.name
        const val GENERAL_ERROR_CODE = 499

        private const val URL_DETAIL = "&method=flickr.photos.getInfo&photo_id="
    }

    private var apiKey: String = BuildConfig.FLICKR_API_KEY

    suspend fun getPhotosFromApi(query: String, page: Int, perPage: Int): Result<ArrayList<Photo>> {
        var result: Result<ArrayList<Photo>> = handleSuccess(arrayListOf())
        try {
            val response = apiService.getPhotos(query, apiKey, page, perPage)
            response?.let {
                it.body()?.photos?.let { photosResponse ->
                    result = handleSuccess(photosResponse.photo)
                }
                it.errorBody()?.let { responseErrorBody ->
                    if (responseErrorBody is HttpException) {
                        responseErrorBody.response()?.code()?.let { errorCode ->
                            result = handleException(errorCode)
                        }
                    } else result = handleException(GENERAL_ERROR_CODE)
                }
            }
        } catch (error: HttpException) {
            return handleException(error.code())
        } catch (e: JsonSyntaxException) {
            return handleException(GENERAL_ERROR_CODE)
        }
        return result
    }

    suspend fun getDetailFromApi(photoId: String): Result<PhotoDetail>? {
        var result: Result<PhotoDetail>? = null
        try {
            val response = apiService.getPhotoDetail(photoId, apiKey)
            response?.let {
                it.body()?.photo?.let { photosResponse ->
                    result = handleSuccess(photosResponse)
                }
                it.errorBody()?.let { responseErrorBody ->
                    if (responseErrorBody is HttpException) {
                        responseErrorBody.response()?.code()?.let { errorCode ->
                            result = handleException(errorCode)
                        }
                    } else result = handleException(GENERAL_ERROR_CODE)
                }
            }
            return result
        } catch (error: HttpException) {
            return handleException(error.code())
        }
    }
}
