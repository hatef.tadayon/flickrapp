package com.flikr.app.ui

import com.flikr.app.data.repository.FlickrRepository

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import com.flikr.app.data.Result
import com.flikr.app.data.model.PhotoDetail

class PhotoDetailViewModel(private val photosRepository: FlickrRepository) : ViewModel() {

    private val photoDetail = MutableLiveData<Result<PhotoDetail>>()

    fun getPhotoDetail() = photoDetail

    fun loadData(photoId: String) {
        viewModelScope.launch {
            val response = photosRepository.getDetailFromApi(photoId)
            response.let {
                when (it) {
                    is Result.Success -> {
                        val detail = it.extractData
                        detail?.let { photoDetail ->
                            this@PhotoDetailViewModel.photoDetail.postValue(
                                Result.Success(
                                    photoDetail
                                )
                            )
                        }
                    }
                    else -> photoDetail.postValue(it)
                }
            }
        }
    }

}