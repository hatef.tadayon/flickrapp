package com.flikr.app.ui

import com.flikr.app.data.model.Photo
import com.flikr.app.data.repository.FlickrRepository

import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import com.flikr.app.data.Result

class PhotosViewModel(private val photosRepository: FlickrRepository) : ViewModel() {

    private val photos = MutableLiveData<Result<ArrayList<Photo>>>()
    private var currentPage = 1
    private var perPage = 15
    private var searchQuery = ""
    var listState: Parcelable? = null //to save and restore rv's adapter

    fun getPhotos() = photos

    fun getCurrentPage() = currentPage

    private fun loadData(searchQuery: String) {
        if (currentPage == 1) {
            photos.postValue(Result.InProgress)
        }
        viewModelScope.launch {
            val response = photosRepository.getPhotosFromApi(searchQuery, currentPage, perPage)
            response.let {
                when (it) {
                    is Result.Success -> {
                        val photosList = it.extractData
                        photosList?.let { list ->
                            if (currentPage == 1) { //set photos for first page
                                photos.postValue(Result.Success(list))
                            } else { //add photos to current list
                                val currentPhotos: ArrayList<Photo>? = photos.value?.extractData
                                if (currentPhotos == null || currentPhotos.isEmpty()) {
                                    photos.postValue(Result.Success(list))
                                } else {
                                    currentPhotos.addAll(list)
                                    photos.postValue(Result.Success(currentPhotos))
                                }
                            }
                        }
                    }
                    else -> photos.postValue(it)
                }
            }
        }
    }

    fun loadDataNextPage() {
        currentPage++
        loadData(this.searchQuery)
    }

    fun setSearchQuery(searchQuery: String) {
        this.searchQuery = searchQuery
        photos.postValue(Result.NewSearch)
        loadData(searchQuery)
    }
}