package com.flikr.app.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flikr.app.data.api.ApiService
import com.flikr.app.data.repository.FlickrRepository

class ViewModelFactory(private val apiService: ApiService) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhotosViewModel::class.java)) {
            return PhotosViewModel(FlickrRepository(apiService)) as T
        }
        if (modelClass.isAssignableFrom(PhotoDetailViewModel::class.java)) {
            return PhotoDetailViewModel(FlickrRepository(apiService)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}