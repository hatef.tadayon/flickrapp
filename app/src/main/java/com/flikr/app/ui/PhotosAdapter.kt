package com.flikr.app.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.flikr.app.DetailFragment
import com.flikr.app.R
import com.flikr.app.data.model.Photo
import com.flikr.app.databinding.RowPhotoHomeBinding
import com.flikr.app.utils.setImageFromUrlWithProgressBar


class PhotosAdapter(private val photosList: ArrayList<Photo>) :
    RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RowPhotoHomeBinding.inflate(inflater, parent, false)
        return PhotosViewHolder(binding)
    }

    override fun getItemCount() = photosList.size

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        holder.bind(photosList[position])
    }

    class PhotosViewHolder(rowBinding: RowPhotoHomeBinding) :
        RecyclerView.ViewHolder(rowBinding.root) {
        private val binding = rowBinding

        fun bind(photo: Photo) {
            binding.photoBinding = photo
            binding.executePendingBindings()
            binding.rowPhotoImg.setImageFromUrlWithProgressBar(
                photo.getImageUrl(),
                binding.rowPhotoProgress
            )
            binding.root.setOnClickListener { view ->
                val bundle = bundleOf(DetailFragment.PHOTO_ARG to photo)
                view.findNavController().navigate(R.id.SecondFragment, bundle)
            }
        }
    }
}