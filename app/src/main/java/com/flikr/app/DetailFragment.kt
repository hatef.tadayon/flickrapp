package com.flikr.app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.flikr.app.data.Result
import com.flikr.app.data.api.ApiService
import com.flikr.app.data.api.RetrofitService
import com.flikr.app.data.model.Photo
import com.flikr.app.databinding.FragmentDetailBinding
import com.flikr.app.ui.PhotoDetailViewModel
import com.flikr.app.ui.ViewModelFactory
import com.flikr.app.utils.setImageFromUrl


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class DetailFragment : Fragment() {
    companion object {
        private val TAG = DetailFragment::class.java.name
        const val PHOTO_ARG = "PHOTO_ARG"
    }
    private var photoDetailViewModel: PhotoDetailViewModel? = null
    private lateinit var binding: FragmentDetailBinding
    private lateinit var photo: Photo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadArguments()
    }

    private fun loadArguments() {
        arguments?.getParcelable<Photo>(PHOTO_ARG)?.let {
            photo = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this@DetailFragment
        binding.toolbar.title = photo.title
        binding.toolbar.setNavigationIcon(R.drawable.ic_close)
        binding.toolbar.setNavigationOnClickListener {
            view?.findNavController()?.popBackStack()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.photoDetailsImage.setImageFromUrl(photo.getImageUrl())
        initViewModels()
        initObservers()
    }

    private fun initViewModels() {
        if (null == photoDetailViewModel) {
            photoDetailViewModel = ViewModelProvider(
                this@DetailFragment,
                ViewModelFactory(RetrofitService.createService(ApiService::class.java))
            ).get(
                PhotoDetailViewModel::class.java
            )

        }
    }

    private fun initObservers() {

        photoDetailViewModel?.getPhotoDetail()?.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Result.Success -> {
                    binding.photoBinding = result.data
                    binding.photoDetailsProgress.visibility = View.GONE
                }
                is Result.InProgress -> {
                    binding.photoDetailsProgress.visibility = View.VISIBLE
                }
                is Result.Error -> {
                    binding.photoDetailsProgress.visibility = View.GONE
                    binding.photoDetailsImage.setImageDrawable(
                        ContextCompat.getDrawable(
                        context!!, // Context
                        R.drawable.ic_close // Drawable
                    ))
                    Toast.makeText(activity, result.exception.message, Toast.LENGTH_LONG).show()
                }
            }
        })
        photoDetailViewModel?.loadData(photo.id)
    }

}